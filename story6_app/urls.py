from django.urls import path
from . import views

urlpatterns = [
    path('', views.story, name='story'),
    path('delete', views.delete),
]