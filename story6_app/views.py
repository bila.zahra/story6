from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import MessageForm
from .models import Message
from django.utils import timezone
import datetime
# Create your views here.

def story(request):
    form = MessageForm()
    if request.method =="POST":
        form=MessageForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story')
    stories = Message.objects.order_by("datetime").reverse()
    return render(request, 'index.html',{
        'story' : stories,
        'server_time' : timezone.now(),
        'form':form,
        })
    
def delete(request):
    if request.method == "POST":
        Message.objects.get(id=request.POST['id']).delete()
    return redirect('story')
