# Generated by Django 2.0.6 on 2019-11-04 11:32

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='Yoga Mahendra', max_length=50)),
                ('location', models.CharField(blank=True, max_length=50)),
                ('subject', models.CharField(blank=True, max_length=50)),
                ('messages', models.CharField(max_length=255)),
                ('datetime', models.DateTimeField(blank=True, default=datetime.datetime.now)),
            ],
        ),
    ]
