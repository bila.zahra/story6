from django.test import TestCase

# Create your tests here.
import datetime
import time

from django.test import Client, LiveServerTestCase, TestCase
from django.urls import resolve
from django.utils import timezone

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from .models import Message
from .views import delete, story

# Django Test Cases
class StoryTest(TestCase):
    def test_story_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_story_view(self):
        found = resolve('/') 
        self.assertEqual(found.func, story)

    def test_using_story_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
        
    def test_form_exists(self):
        response = Client().get('/')
        self.assertIn('</form>', response.content.decode())
        
    def test_add_remove_messages(self):
        Message.objects.create(name="Test", location="Kampus", subject="Subject", messages="The message")
        test = Message.objects.filter(name="Test")
        self.assertTrue(test)
        test.delete()
        test = Message.objects.filter(name="Test")
        self.assertFalse(test)

# Selenium Test Cases    
class MessageTestSelenium(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(MessageTestSelenium, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(MessageTestSelenium, self).tearDown()

    def test_upload_and_delete_story(self):
        # Opening the link we want to test
        self.browser.get('http://127.0.0.1:8000/')

        # find the form element
        time.sleep(3)
        name = self.browser.find_element_by_id('id_name')
        location = self.browser.find_element_by_id('id_location')
        subject = self.browser.find_element_by_id('id_subject')
        messages = self.browser.find_element_by_id('id_messages')
        submit = self.browser.find_element_by_id('id_submit')
        # Fill the form with da ta
        name.send_keys('Nabila Azzahra')
        location.send_keys('Kampus')
        subject.send_keys('Budak Korporat')
        messages.send_keys('Kongres ngeselin!')
        # submitting the form
        submit.click()
        time.sleep(3)
        # check the returned result
        assert 'Nabila Azzahra' in self.browser.page_source
        assert 'Kampus' in self.browser.page_source
        assert 'Budak Korporat' in self.browser.page_source
        assert 'Kongres ngeselin!' in self.browser.page_source
        # deletes the uploaded story
        delete = self.browser.find_elements_by_class_name('button-story')
        delete[0].click()
        assert 'Nabila Azzahra' not in self.browser.page_source
        assert 'Kampus' not in self.browser.page_source
        assert 'Budak Korporat' not in self.browser.page_source
        assert 'Kongres ngeselin!' not in self.browser.page_source
        time.sleep(3)